cmake_minimum_required(VERSION 3.6)
project(ft_printf)

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wall -Wextra -Werror")

SET(SOURCE_FILES_LIB
	src/ft_ftoa.c
	src/ft_ftoa_s.c
	src/ft_itoa.c
	src/ft_ltoa_base.c
	src/ft_tolower.c
	src/ft_toupper.c
	src/ft_ulltoa_base.c
	src/ft_wcstombs.c
	src/ft_wctomb.c
	src/ft_bzero.c
	src/ft_memalloc.c
	src/ft_memset.c
	src/ft_memcpy.c
	src/ft_isdigit.c
	src/ft_strcat.c
	src/ft_strchr.c
	src/ft_strcpy.c
	src/ft_strdel.c
	src/ft_strdup.c
	src/ft_strjoin.c
	src/ft_strlen.c
	src/ft_strncat.c
	src/ft_strncmp.c
	src/ft_strncpy.c
	src/ft_strnequ.c
	src/ft_strnew.c
	src/ft_abs.c
	src/ft_pow.c
	src/ft_powf.c
	src/ft_round.c)

set(SOURCE_FILES
		src/ft_printf.c
		src/ft_putfld.c
		src/ft_c_s_lc_ls_di.c
		src/ft_n_p_boux_ldlolu.c
		src/ft_f.c
		src/ft_tobs.c
		src/pars_format.c
		src/ft_xprintf.c
		src/ft_undefspec.c
		src/helpers.c
		src/ft_colors.c
		src/ft_output.c)

include_directories(${CMAKE_CURRENT_SOURCE_DIR}/lib/includes ${CMAKE_CURRENT_SOURCE_DIR})

add_library(ftprintf ${SOURCE_FILES} ${SOURCE_FILES_LIB})


unset(CMAKE_C_FLAGS)

set(CMAKE_C_FLAGS "-Wno-format-invalid-specifier -Wno-format -Wno-macro-redefined -Wno-implicitly-unsigned-literal")
add_executable(basic_tests tests/basic_tests.c)
add_executable(printf_main tests/printf_main2.c)
target_link_libraries(basic_tests ftprintf)
target_link_libraries(printf_main ftprintf)
