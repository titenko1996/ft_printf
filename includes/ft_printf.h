/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/09 22:41:25 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:14:33 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_FT_PRINTF_H
# define FT_PRINTF_FT_PRINTF_H
# include <stdlib.h>
# include <stdarg.h>
# include <inttypes.h>

# define FSP			0x01
# define FPL			0x02
# define FMI			0x04
# define FNO			0x08
# define FZE			0x10

# define NPCOLOR		0x01
# define PCOLOR			0x02
# define FG				0x04
# define BG				0x08

# define BLACK			0
# define RED			1
# define GREEN			2
# define YELLOW			3
# define BLUE			4
# define MAGENTA		5
# define CYAN			6
# define WHITE			7

typedef unsigned long long	t_ullong;
typedef unsigned long		t_ulong;
typedef long long			t_llong;

typedef struct				s_format
{
	union					u_v
	{
		t_llong				li;
		long double			ld;
	}						v;
	char					*s;
	int						n0;
	int						nz0;
	int						n1;
	int						nz1;
	int						n2;
	int						nz2;
	int						prec;
	int						width;
	int						nchar;
	unsigned int			flags;
	char					qual;
	char					qual1;
	int						color;
	char					colorf;
}							t_format;

const char					*ft_find_perc(const char *fmt);
int							ft_putn(const char *fmt, size_t n, t_format *x);
void						ft_pad(const char *s, size_t n, t_format *x);
void						ft_parse_flags(const char **s, t_format *x);
int							ft_get_num(const char **s, va_list ap);
void						ft_get_width_arg(const char **s, t_format *x,
												va_list ap);
void						ft_get_prec_arg(const char **s, t_format *x,
											va_list ap);
void						ft_parse_qual(const char **s, t_format *x);
int							parse_num(const char **s);

int							ft_xprintf(const char *fmt, va_list ap);
int							ft_printf(char *fmt, ...);
void						ft_putfld(t_format *px, va_list pap, char code,
										char *ac);

void						ft_litob(t_format *px, char code);
void						ft_ldtob(t_format *px, char code);

void						ft_lc(t_format *px, va_list pap, char code);
void						ft_ls(t_format *px, va_list pap, char code);
void						ft_s(t_format *px, va_list pap, char code);
void						ft_c(t_format *px, va_list pap, char code,
									char *ac);
void						ft_di(t_format *px, va_list pap, char code,
									char *ac);
void						ft_boux(t_format *px, va_list pap, char code,
									char *ac);
void						ft_p(t_format *px, va_list pap, char code,
									char *ac);
void						ft_n(t_format *px, va_list pap, char code);
void						ft_ldou(t_format *px, va_list pap, char code,
									char *ac);
void						ft_fe(t_format *px, va_list pap, char code,
									char *ac);
void						ft_undefspec(t_format *px, char code, char *ac);
char						*ft_strtoupper(char *ac);
void						ft_parse_color(const char **s, t_format *pxx,
											va_list pap);
void						ft_color(t_format *px);
void						ft_output(t_format *px, char *ac);

# define MAX_PAD		(sizeof (g_spaces) - 1)

#endif
