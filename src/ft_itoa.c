/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/03 12:17:25 by dtitenko          #+#    #+#             */
/*   Updated: 2016/12/03 13:09:37 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_strings.h"

static int	countlen(int n)
{
	int i;

	i = 0;
	if (n == 0)
		return (1);
	if (n < 0)
		i++;
	while (n)
	{
		n /= 10;
		i++;
	}
	return (i);
}

char		*ft_itoa(int n)
{
	char			*str;
	char			*p;
	int				len;
	unsigned int	nn;

	len = countlen(n);
	if (!(str = ft_strnew(len)))
		return (NULL);
	p = str + len;
	if (0 > n)
	{
		*str = '-';
		nn = -n;
		len--;
	}
	else
		nn = n;
	while (len--)
	{
		*--p = '0' + (nn % 10);
		nn /= 10;
	}
	return (str);
}
