/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcpy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/24 14:31:49 by dtitenko          #+#    #+#             */
/*   Updated: 2016/11/26 19:24:49 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_memory.h"

void	*ft_memcpy(void *dest, const void *src, size_t n)
{
	t_uchar	*s;
	t_uchar	*d;

	s = (unsigned char *)src;
	d = (unsigned char *)dest;
	while (n--)
		*d++ = (unsigned char)*s++;
	return ((unsigned char *)dest);
}
