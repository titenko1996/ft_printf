/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ulltoa_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/28 23:29:21 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:16:00 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_convert.h"

char	*ft_ulltoa_base(unsigned long long value, int base)
{
	char				*res;
	int					len;
	unsigned long long	tmp;

	len = get_size_for_base(value, base);
	tmp = value;
	len -= ((long long)value < 0 && base == 10) ? 1 : 0;
	if (!((res = (char *)malloc(len + 1)) && base >= 2 && base <= 36))
		return (char *)0;
	res[len] = '\0';
	value == 0 ? res[0] = '0' : 0;
	while (tmp)
	{
		--len;
		res[len] = get_base_char(tmp % base);
		tmp /= base;
	}
	return (res);
}
