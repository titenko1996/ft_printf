/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_xprintf.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/09 22:45:48 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/14 19:59:31 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_strings.h"
#include "includes/ft_printf.h"
#include <unistd.h>

const char	*ft_find_perc(const char *fmt)
{
	const char *s;

	s = fmt;
	while (*s != '{' && *s != '%' && *s)
		s++;
	return (s);
}

void		ft_parse_perc(t_format *px, va_list pap, const char **s)
{
	(*s)++;
	ft_parse_flags(s, px);
	ft_get_width_arg(s, px, pap);
	ft_parse_flags(s, px);
	ft_get_prec_arg(s, px, pap);
	ft_parse_flags(s, px);
	ft_parse_qual(s, px);
	ft_parse_flags(s, px);
}

int			ft_xprintf(const char *fmt, va_list ap)
{
	t_format	x;
	const char	*s;
	char		ac[68];

	x.nchar = 0;
	while (1)
	{
		s = ft_find_perc(fmt);
		if ((!ft_putn(fmt, s - fmt, &x) && (*s) != '%' && (*s) != '{')
			|| !(*s) || !(*(s + 1)))
			return (x.nchar);
		x.flags = 0;
		((*s) == '{') ? ft_parse_color(&s, &x, ap) : (x.colorf = 0);
		((*s) == '%') ? ft_parse_perc(&x, ap, &s) : 0;
		ft_putfld(&x, ap, *s, ac);
		ft_output(&x, ac);
		if (ft_strchr("SCfF", *s) || x.colorf & PCOLOR
			|| (ft_strchr("sc", *s) && x.qual == 'l'))
			ft_strdel(&(x.s));
		fmt = s + 1;
	}
}
