/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helpers.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/09 21:49:39 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:14:52 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_strings.h"
#include "includes/ft_convert.h"

char	*ft_strtoupper(char *ac)
{
	char	*tmp;
	size_t	len;
	int		i;

	len = ft_strlen(ac);
	i = 0;
	if (!(tmp = ft_strnew(len)))
		return (NULL);
	while (len--)
	{
		*(tmp + i) = (char)ft_toupper(*(ac + i));
		i++;
	}
	return (tmp);
}

int		parse_num(const char **s)
{
	int num;

	num = 0;
	while (ft_isdigit(**s))
	{
		num = num * 10 + **s - '0';
		(*s)++;
	}
	return (num);
}
