/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tobs.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/09 22:59:37 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:14:41 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_printf.h"
#include "includes/ft_strings.h"
#include "includes/ft_convert.h"
#include "includes/ft_math.h"
#include "includes/ft_memory.h"

static int	ft_detect_base(char code)
{
	int base;

	base = (ft_strchr("oO", code)) ? 8 : 0;
	base = (base == 0 && ft_strchr("xX", code)) ? 16 : base;
	base = (base == 0 && code == 'b') ? 2 : base;
	base = (base == 0) ? 10 : base;
	return (base);
}

void		ft_litob(t_format *px, char code)
{
	int					base;
	char				*ac;
	unsigned long long	uval;
	char				*tmp;

	base = ft_detect_base(code);
	uval = (*px).v.li;
	if (ft_strchr("di", code) && (t_llong)(*px).v.li < 0)
		uval = -uval;
	ac = ((*px).prec || uval) ? ft_ulltoa_base(uval, base) : ft_strnew(1);
	if (code == 'X')
	{
		tmp = ac;
		ac = ft_strtoupper(tmp);
		ft_strdel(&tmp);
	}
	(*px).n1 = (int)ft_strlen(ac);
	ft_memcpy((*px).s, ac, (*px).n1 + 1);
	ft_strdel(&ac);
	if ((*px).n1 < (*px).prec)
		(*px).nz0 = (*px).prec - (*px).n1;
	if ((*px).prec < 0 && ((*px).flags & (FMI | FZE)) == FZE
		&& 0 < (base = (*px).width - (*px).n0 - (*px).nz0 - (*px).n1))
		(*px).nz0 += base;
}

void		ft_ldtob(t_format *px, char code)
{
	char		*ac;
	long double	ldval;
	int			pad;

	ldval = (*px).v.ld;
	if ((*px).prec < 0)
		(*px).prec = 6;
	if (ldval < 0)
		ldval = -ldval;
	ldval = (!((*px).prec) && (code == 'e' || code == 'E')) ?
			ft_roundl(ldval, (unsigned int)(*px).prec + 1) :
			ft_roundl(ldval, (unsigned int)(*px).prec);
	ac = ft_strchr("fF", code) ? ft_ftoa(ldval, (*px).prec, (*px).flags & FNO) :
		ft_ftoa_s(ldval, (*px).prec, (*px).flags & FNO);
	(*px).s = (code == 'E') ? ft_strtoupper(ac) : ft_strdup(ac);
	ft_strdel(&ac);
	(*px).n1 = (int)ft_strlen((*px).s);
	if (((*px).flags & (FMI | FZE)) == FZE
		&& 0 < (pad = (*px).width - (*px).n0 - (*px).nz0 - (*px).n1))
		(*px).nz0 += pad;
}
