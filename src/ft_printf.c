/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/09 21:49:39 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:14:23 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_printf.h"
#include "includes/ft_strings.h"
#include <stdarg.h>
#include <unistd.h>

int	ft_printf(char *fmt, ...)
{
	int		ans;
	va_list	ap;

	va_start(ap, fmt);
	ans = ft_xprintf(fmt, ap);
	va_end(ap);
	return (ans);
}
