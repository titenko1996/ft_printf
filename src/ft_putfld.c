/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putfld.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 19:18:30 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/14 19:59:21 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_printf.h"
#include "includes/ft_strings.h"

void	ft_putfld(t_format *px, va_list pap, char code, char *ac)
{
	(*px).n0 = 0;
	(*px).nz0 = 0;
	(*px).n1 = 0;
	(*px).nz1 = 0;
	(*px).n2 = 0;
	(*px).nz2 = 0;
	if (ft_strchr("nsSpdDioOuUxXcCbfF", code))
	{
		ft_c(px, pap, code, ac);
		ft_di(px, pap, code, ac);
		ft_lc(px, pap, code);
		ft_ls(px, pap, code);
		ft_s(px, pap, code);
		ft_boux(px, pap, code, ac);
		ft_p(px, pap, code, ac);
		ft_ldou(px, pap, code, ac);
		ft_n(px, pap, code);
		ft_fe(px, pap, code, ac);
	}
	else if ((*px).colorf & PCOLOR)
		ft_color(px);
	else
	{
		ft_undefspec(px, code, ac);
	}
}
