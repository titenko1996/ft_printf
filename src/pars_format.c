/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pars_format.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: dtitenko <dtitenko@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/05 02:58:17 by dtitenko          #+#    #+#             */
/*   Updated: 2017/04/09 23:14:56 by dtitenko         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/ft_printf.h"
#include "includes/ft_strings.h"

static const char			g_fchar[] = {" +-#0"};
static const unsigned int	g_fbit[] = {FSP, FPL, FMI, FNO, FZE, 0};

int		ft_get_num(const char **s, va_list ap)
{
	int num;
	int sign;

	sign = (**s == '-') ? -1 : 1;
	(**s == '-') ? (*s)++ : 0;
	if (*(*s) == '*')
	{
		num = va_arg(ap, int);
		(*s)++;
		if (ft_isdigit(**s))
			num = parse_num(s);
	}
	else
	{
		num = parse_num(s);
		if (**s == '*')
		{
			num = va_arg(ap, int);
			(*s)++;
		}
	}
	return (sign * num);
}

void	ft_get_width_arg(const char **s, t_format *x, va_list ap)
{
	(*x).width = ft_get_num(s, ap);
	if ((*x).width < 0)
	{
		(*x).width = -(*x).width;
		(*x).flags |= FMI;
	}
}

void	ft_get_prec_arg(const char **s, t_format *x, va_list ap)
{
	if (**s != '.')
		(*x).prec = -1;
	while (**s == '.')
	{
		(*s)++;
		if ((**s == '-' && !ft_isdigit(*((*s) + 1))) || ft_strchr(" +#0", **s))
			ft_parse_flags(s, x);
		(*x).prec = ft_get_num(s, ap);
		if ((*x).prec < 0)
		{
			(*x).width = -(*x).prec;
			(*x).prec = -1;
			(*x).flags |= FMI;
		}
	}
}

void	ft_parse_qual(const char **s, t_format *x)
{
	(*x).qual = '\0';
	while (ft_strchr("hljzL", (**s)))
	{
		(*x).qual = ft_strchr("hljzL", (**s)) ? *(*s)++ : '\0';
		(*x).qual1 = '\0';
		ft_parse_flags(s, x);
		if (ft_strchr("hl", (*x).qual))
			(*x).qual1 = ft_strchr("hl", **s) ? *(*s)++ : '\0';
	}
}

void	ft_parse_flags(const char **s, t_format *x)
{
	const char *t;

	while ((t = ft_strchr(g_fchar, **s)) && !(*t == '0' && *(t + 1) == '.'))
	{
		(*x).flags |= g_fbit[t - g_fchar];
		(*s)++;
	}
}
